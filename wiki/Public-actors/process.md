Creating non-trivial public actors is a long-term commitment to a solid process and high code quality. In this article, we will go through the necessary steps that every non-trivial public actor should go through.

### 1. Use-case gathering
In the first phase, an Apify expert should gather as much feedback from various sources about what are users and potential customers interested in. Of course, this is a never-ending process as new use-cases and possible solutions will spawn over time. Both Apify experts or actor developers can add their own ideas.

Sources for inspiration:
- [Apify Productboard](https://portal.productboard.com/apify)
- Previous and current customer requests (email, Intercom, Apify marketplace etc.)
- Often searched terms on Google
- Apify experts and developers ideas

### 2. Specification (prioritizing)
Once we gather enough use-cases, Apify experts need to pick the most interesting ones and create a solid specification. Of course, this specification needs to be flexible enough to accommodate any reasonable or necessary changes later.

Each actor specification should be very specific, targeting only a smaller subset of the overall possibilities. It is always better to split the project into multiple phases where between each phase, the actor is published and updated and there is time to gather feedback from the users.

### 3. Actor creation
The developer should code the actor to his best skills, following the specification and proper structure. The developer should ask for 2 reviewers from the Apify team and follow the [checklist](public-actors/checklist).

Once everything is approved, the actor can be published to [Apify store](https://apify.com/store).

### 4. Maintenance and updates
Ideally, the actor will be maintained by the same developer. Of course, this cannot be ensured so clean code and solid documentation should allow transitioning of the "ownership" to another developer.

There will be 2 types of maintenance:
- **Bug fixes and small updates** - These will come from users in the Apify app but also through Github issues and other means. Bugs should be fixed in a matter of a few days (critical ones ideally immediately). For small proposals, they can be included immediately or postponed for a bigger release. They should not contain breaking changes.
- **Bigger updates** - These will come when adding new use-cases, or refactoring the codebase and improving performance. They can contain breaking changes. The documentation needs to be updated accordingly. 

#### Update or create new 
There is an obvious question if the developer should prefer adding more functionality to the current actor or create a new one. This doesn't have a strict answer but generally, if the new use-case is not related to the old functionality, a new actor should be created. But if there is a lot of shared code, it is never good to have code duplication. In that case, the developer can either go with one bigger actor or creating a separate library for the shared functionality.

One example of such an approach is a split between a generic [apify-google-auth](https://www.npmjs.com/package/apify-google-auth) library for authentication and [Google Sheets actor](https://apify.com/lukaskrivka/google-sheets) that is calling this library. This library can be easily reused for any Google service actor in the future.

Since the best practices are still evolving, we will be very glad for any feedback.
