This document contains guidelines and best practices for development and publishing of public actors.

## Structure

```
src/
test/
.editorconfig
.eslintrc
.gitignore
INPUT_SCHEMA.JSON
package.json
package-lock.json
README.md
LICENSE.md
```

All source javascript files belong to `src` directory and tests to `test` directory. Don't put anything into the root because then you can't easily search the source code.

Always start your project using [Apify CLI](https://www.apify.com/docs/cli). This way the project will be initialized using correct `.gitignore` and possibly other files.

### .editorconfig

Always use `.editorconfig` file to ensure that at least the same indentation and charset will be used across the project. 

Recommended configuration:

```
root = true

[*]
indent_style = space
indent_size = 4
charset = utf-8
trim_trailing_whitespace = true
insert_final_newline = true
end_of_line = lf
# editorconfig-tools is unable to ignore longs strings or urls
max_line_length = null
```

### .eslintrc

For important and larger project use [ESLint](https://eslint.org) to ensure for example:
* That no undefined variables will be used
* Beware of common errors such as iteration over the class instance etc.
* Code style will be consistent across the project

Recommended configuration:

```json
{
    "extends": ["@apify"]
}
```

In addition to that you must install following packages as development dependencies:

```bash
npm install --save-dev eslint @apify/eslint-config
```

### .gitignore

Git ignore should contain `node_modules` and `apify_storage`.

### package.json

Packages that are required for the actor to run (not to be tested) belong to `dependencies` in package.json file. Packages that are only needed for development (ESLint) and testing (Mocha) belong to `devDependencies`.

Always set `scripts.start` property to point to the bootstraping file of your actor located in `src` directory (for example `src/main.js`). For example:

```json
{
    ...

    "scripts": {
        "start": "node src/main.js"
    }

    ...
}
```

### package-lock.json

Package lock should be always committed to have versions fixed. Versions should be updated in regular intervals.

### LICENSE.md

Apify opensource software is always licensed under [Apache license](https://raw.githubusercontent.com/apifytech/apify-js/master/LICENSE.md). Include the copy in `LICENSE.md` and make sure that same license is in `package.json`.

## Development

* Public actors should be hosted Github under the ApifyTech account (http://github.com/apifytech/apify-js) and name should be prefixed with `actor-` as `actor-[your-act-name]`.

## Code

* Keep the main file as small as possible - only the main logic inside.
* Functions belong to separate files.

## Proxy

Here is a summary of a discussion we had around proxy availability:

We should make the proxy situation with new users as smooth as possible. The current problem is that some actors have hardcoded proxy and free users cannot use it or they are confused by that. There are 2 main points:
* Make the situation with proxies as visible as possible. If there is need for proxies, it should shout at the user in the readme and the log. Having a basic error "Proxy is needed" or just failing is not enough.
* If there isn't an absolute necessity for a specific proxy, you should allow the user to fall back no non-proxy if he chose automatic with a visible warning. Basically, he should be able to test a few pages without a proxy. He needs to get to the results asap. There is an API endpoint that tells you if a user has available proxies or not.