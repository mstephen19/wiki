Before you publish an important public actor, you should fulfill this checklist. For smaller public actors, you can get out with just a simple README.md.

- **README.md** - Detailed and well structured (check [Instagram Scraper](https://apify.com/jaroslavhejlek/instagram-scraper) and [Google Sheets](https://apify.com/lukaskrivka/google-sheets) for inspiration).
- **Hosted on Github** - Can be hosted
- [**Use Apify EsLint**](https://github.com/apifytech/apify-eslint-config)
- [**Use INPUT_SCHEMA**](https://apify.com/docs/actor/input-schema)
- **Well structured** - Check this [article](Public-actors/structure)
- **Example run** - Should cover the latest version and common use-case.
- **Testing actor** - You should build testing actor that will run the main actor on schedule, test it and notify if it fails the tests.
- **Nice logs** - Logs should be readable and friendly also for non-developers. Avoid flooding the log with unimportant messages. (Check for inspiration - [Google Sheets](https://apify.com/lukaskrivka/google-sheets?section=example-run)).
- **Debug logs** - Some logs that are only relevant for testing should be coded as debug logs, only displayed in development.
- **2 people review** - Find your reviewers on Apify Slack. Find them early, it takes time.
- **Nice code** - Code should be clean and well structured. Think that other people will read it and build upon it. Split the code into well-named files and functions.
- **SEO** - Title, description, and README.md should be SEO friendly to promote the actor. Read more [in this article](https://kb.apify.com/en/articles/2644024-seo-for-actors).
- **Test as a new user** - Try to approach your actor as a completely new user. Remember that it is written for others to use.
- Use proper versioning - *Needs to be discussed by Apify team*
- **Measure CU usage** -  for basic use-cases and create a table for it in the README.md
- **Blog post**(optional) - Write a blog post on Medium that will introduce the actor and guide the users through basic use-case.
- **Tweet** - Done by Apify marketing team.
- **Newsletter** - Done by Apify marketing team.