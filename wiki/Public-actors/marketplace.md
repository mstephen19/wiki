Public actors are the first thing a new lead sees on our [website](https://apify.com). In the past, our internal team members were creating the most important ones. Unfortunately, this is unsustainable as writing such an actor takes a lot of time. We also don't want the [store](https://apify.com/store) to be built by just a few chosen developers, we aim it to be Airbnb for software development.

That's why we decided to offer you cooperation on creating important public actors for our users. It will work similarly as usual customer projects, every now and then we will submit new public actor project on the marketplace for the fixed price and you can pick it (it may become even part of the future bidding system). By creating public actors you can build up your reputation and be seen as a more valuable and experienced developer on the marketplace.

### There are a few differences to customer projects through:
- Public actors are a more long-term project. We expect you to stick with the project for at least a few months.
- Code quality has higher priority. The code will be public on Github and the actors might be the first and critical contact of a new user with Apify
- Documentation is critical. It should be clear, detailed and readable.
- Code should be open to changes and updates. Don't fear to refactor.
- Periodic testing is also critical. You might not have any customer to test it for you and get back to you.

### How it will work
- A new project will be put on the marketplace for a fixed price
- Once it is approved, you will get paid and the project will move to the maintenance phase
- You can write a blog post to earn extra $$$. Will be paid hourly.
- We may create a new project for a fixed price to add more functionality to an existing actor. We will prefer you as an author but we may put it into the marketplace if you are not available.
- There will be small requests to updates and bug fixes. Small updates and bug fixes will be charged by an agreed hourly rate. You will need to report what were the hours spent on.
- If we are really unhappy with you developing the project, we may "transfer" it to someone else (by doing a fork). But this should happen really rarely.

### What do you need to start
- Read all the articles in the Public actors section. 
- Make sure you have enough time long term to maintain the project (we expect 0-3 hours weekly).
- Make sure the hourly rate works for you long term.
- Commit to high code quality. Shortcuts and dirty solutions only when you really don't have another idea.
- Look at some actors in the [store](https://apify.com/store) for inspiration
- Read some [blogs](https://blog.apify.com/) to get familiar with how we think and write about actors, e.g. [Google Sheets](https://blog.apify.com/import-data-easily-to-and-from-google-sheets-with-a-new-apify-actor-43536b719029) (long one), [Kickstarter](https://blog.apify.com/kickstarter-search-actor-create-your-own-kickstarter-api-7672acdb8d77) (mid-size), [Content Checker](https://blog.apify.com/how-to-set-up-a-content-change-watchdog-for-any-website-in-5-minutes-460843b12271) (short)