It's always convenient to have all the information you need for coding available directly in your IDE. Since we're all working with Puppeteer all day long, having access to its docs with a press of a button saves time and frustration. Let's look at making it happen in WebStorm (feel free to add other IDEs if you know the process).

## Webstorm
First, we have to install the type information.
```
npm install @types/puppeteer --save-dev
```

Afterwards, we just need to tell WebStorm the types of individual variables. For that we will use [JSDoc](http://usejsdoc.org/) comments. It's also a great incentive to actually comment your code properly.

### Functions
Create a JSDoc comment above any function that uses `page` as an argument and add a `{Page}` type to it. Voila, WebStorm automatically picks up the type of the variable and offers code completion.

![Screenshot_2018-11-13_at_11.43.59](uploads/ec51547fdf6928624227ad3773befc8a/Screenshot_2018-11-13_at_11.43.59.png)

You can even press `F1` while your cursor is somewhere within the function's name to get the full docs.

![Screenshot_2018-11-13_at_11.45.43](uploads/1df18e3a7585307b6634116086c68b01/Screenshot_2018-11-13_at_11.45.43.png)

### Variables
Sometimes you just want to tell WebStorm that a specific variable is actually a Puppeteer `Page`. To do that, simply add a JSDoc `@type` annotation. As you can see in the screenshot, if you annotate the variable, WebStorm will assume that the variable is of the given type, even if that is syntactically impossible (the `const page;` is `undefined` and cannot be reassigned since it's a `const`), so be careful with your types.

![Screenshot_2018-11-13_at_11.53.04](uploads/999a06b11b6027737b190b5abf96be47/Screenshot_2018-11-13_at_11.53.04.png)

#### Troubleshooting
See the screenshot how the code looks without the `@types/puppeteer` installed. In the JSDoc comment, `Page` is marked as an unknown identifier and in the code, `page.exposeFunction(handle, func)` is underlined, telling us that WebStorm cannot resolve the property.

![Screenshot_2018-11-13_at_11.30.00](uploads/f1b16eb0c44f3e6c199565ad4be67711/Screenshot_2018-11-13_at_11.30.00.png)

With the types installed, but without the proper JSDoc comment, no code completion will be available.

![Screenshot_2018-11-13_at_11.48.36](uploads/d437637473fe2b684cb211c917e8d019/Screenshot_2018-11-13_at_11.48.36.png)