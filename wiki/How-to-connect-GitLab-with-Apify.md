If you want to use GitLab as a source of code for Apify actor, you need to be at least in the role *Maintainer* and then this is what you need to do.
But keep in mind that after every push you need to build the actor manually.

1. Create a project on GitLab
2. Push the code to the repo, if you want to have more actors in one repo, which makes sense for one customer for example, then just upload one actor to one folder.

![alt text|50%](https://apify-uploads-prod.s3.amazonaws.com/8d609211-53ec-43ca-9025-e166e1866bce_Screenshot2018-11-14at12.08.18.png "Example of folders")

3. When we have the project o GitLab and code here, go to the Apify to the actor where you want to load the code to and change the source to **Git repository** :

![alt text](https://apify-uploads-prod.s3.amazonaws.com/20660f30-3a4a-4ddb-ba13-3418a158a810_Screenshot2018-11-14at12.14.12.png "Source code")

4. From here click on **deployment key** and copy from new window the key
![alt text](https://apify-uploads-prod.s3.amazonaws.com/5701841b-8b05-46d7-b8ef-c8b550e61056_Screenshot2018-11-14at12.14.12.png "Key")

5. Go to the GitLab project to the settings -> Repository -> Deploy Keys and add there the copied public SSH key
![alt text](https://apify-uploads-prod.s3.amazonaws.com/316fd18e-7fac-4acd-b972-aeb11da870c3_Screenshot2018-11-14at12.21.12.png "Deploy Tokens")

6. Now you can copy the git url, use the SSH, which you can switch and copy here on the home of the project:
![alt text](https://apify-uploads-prod.s3.amazonaws.com/c5914f29-ca58-460d-8e42-f2da863ef58e_Screenshot2018-11-14at12.24.39.png "Git url")

7. Go to the actor again and past there the url, but you need to modify this way (always use the #master)

**git@gitlab.com:apify-private-actors/stylar.git#master:name_of_the_folder_you_want_to_load**

So the example for the Asos actor will be: 

**git@gitlab.com:apify-private-actors/stylar.git#master:asos**

And you can click build.

8. Tadaaaaaa
![alt text](https://apify-uploads-prod.s3.amazonaws.com/5880f3bd-432c-4e8e-9bb7-51522b7bd837_Screenshot2018-11-14at13.27.36.png "tadaaaa")