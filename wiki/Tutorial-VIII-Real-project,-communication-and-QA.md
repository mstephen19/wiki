If you got up to here, first, please check if you did not forget any parts of the previous modules. You should push everything to your repo on Gitlab contact us and wait for feedback. Once all the feedback is processed, you can start with the first real project.

### Introduction
You will work on a project either for us or a real customer. This project will be placed in the [marketplace section](https://my.apify.com/work) of our app like a regular real project. It will go through all phases and you will communicate with the customer through the internal communication (which sends notifications on email). Once you finish with the development, you should send a dataset (or any type of output) sample to the customer and then make sure with the customer that everything is correct. After that the project will be copied to the customer's account, it will be invoiced and you will receive your first reward. You will be added to our Slack where we will be ready to help you with any questions.

### Learning
- Go through the [form to request a project](https://apify.com/marketplace). This is the place where new projects are coming from. DO NOT SUBMIT A PROJECT!
- Look how a solid specification should look like [here](https://kb.apify.com/en/articles/3039446-data-extraction-project-request-example), [here](https://kb.apify.com/en/articles/3039638-web-automation-project-request-example) and [here](https://kb.apify.com/en/articles/3039659-something-else-project-request-example).

### Exercise
- Let's build the actor according to the specification of the project. That's it!