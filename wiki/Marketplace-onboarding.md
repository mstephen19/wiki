### Business

1. Check out what’s [Apify about](https://www.apify.com)
2. Go through [case studies](https://www.apify.com/case-studies)
 
### Scraping
 
1. Read about approaches how to scrape websites efficiently [here](https://blog.apify.com/web-scraping-in-2018-forget-html-use-xhrs-metadata-or-javascript-variables-8167f252439c) (uses legacy Crawler product, but principles are still valid)
2. Learn basics of Puppeteer ([a nice example of Puppeteer crawler with Apify SDK](https://www.apify.com/mtrunkat/example-hacker-news)), Cheerio, and jQuery
 
### By-passing anti-scraping solutions
 
1. Check out [this KB article](https://kb.apify.com/tips-and-tricks/several-tips-how-to-bypass-website-anti-scraping-protections)
2. Learn about available proxy groups: TODO link to the new Docs
 
### Actor
 
1. Learn about [Apify SDK](https://sdk.apify.com/)
2. Go through [docs](https://www.apify.com/docs) to learn about storages, scheduler, etc.
3. Check out [Apify CLI](https://kb.apify.com/actor/develop-build-and-run-actors-locally-using-the-apify-command-line-client)
4. Check out trending actors in our [Library](https://www.apify.com/library)
 
### Some general rules
 
* Prefer simple HTTP requests and Cheerio over Puppeteer if not needed
* Use English everywhere, comment code properly
* Publish your actors/crawlers if possible (ask your data expert if you can publish your work in a library)
* When publishing, try to generalize actor to be useful in more cases (variable inputs, more parameters, etc.)

