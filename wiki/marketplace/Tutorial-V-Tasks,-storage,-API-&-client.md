### Introduction

In this module, we will look at a lot of stuff - practically everything that you need to know if you want to integrate Apify actors into your external project/application. Tasks are a very useful feature, which allow us to save pre-configured inputs for actors. Storage allows us to save persistent data for further processing.

To tie it all together, we'll take a look at Apify's API and JavaScript API client, with which we can do anything a regular user can do (but programmatically). In this module, you will see how all of these components are connected.

### Learning

-   Read the docs about [Tasks](https://apify.com/docs/tasks) and [Storage](https://apify.com/docs/storage).
-   Scroll through [API docs](https://apify.com/docs/api/v2) and [API client docs](https://apify.com/docs/api/apify-client-js/latest). The client is also on [Github](https://github.com/apifytech/apify-client-js). (These are pretty long so we don't expect you will memorize everything).
-   Read a few articles about [storage](https://help.apify.com/en/collections/1669761-storing-and-accessing-data) and [API integration](https://help.apify.com/en/articles/2868670-how-to-pass-data-from-web-scraper-to-another-actor) _(Please note that some of these articles are quite old; however, the overall concepts within them are still relevant)_.
-   Default _(unnamed)_ storage is connected with an actor run, [refresh your understanding of actor runs](https://apify.com/docs/actor#run).

### Quiz

1. What is the relationship between actors and tasks?
2. What are the differences between default _(unnamed)_ and named storage? Which one would you use for everyday usage?
3. What is the relationship between the Apify API and Apify client? Are there any significant differences?
4. Is it possible to use a request queue for deduplication of product ids? If yes, how would you do that?
5. What is data retention and how does it work for all types of storages (default and named)?
6. How do you pass input when running actor or task via API?

### Exercise

Get back to your main actor and create a task for it. This time, set any keyword you like.

-   Create a new actor locally that should work the same locally and on Apify platform (push it to Gitlab). This new actor should take these fields on input - `memory` (number), `useClient` (boolean), `fields` (string[]), `maxItems` (number). Example:

```JSON
{
    "memory": 4096, // Memory has to be a power of 2
    "useClient": false,
    "fields": ["title", "itemUrl", "offer"],
    "maxItems": 10
}
```

The job of this new actor should be to call the task you created for your main actor via API or client (depends on `useClient`), wait for it to finish, then extract the data in CSV format and save the CSV to the key value store as OUTPUT.csv.

Be sure to:

-   Match all the input variants to the correct parameters in the calls (one call to run the actor and one to retrieve the data from dataset).
-   Run at least 3 variations of the input (each field should vary few times)
-   Ensure it works the same locally and on platform
