### Introduction

Using the Apify SDK, we are now able to collect and format data coming directly from websites and save it into a Key-Value store or Dataset. This is great, but sometimes, we want to store some extra data about the run itself, or about each request. We might want to store some extra general run information separately from our results, or potentially include statistics about each request within its corresponding dataset item.

### Reading

The types of values that are saved are totally up to you, but the most common are error scores, number of total saved items, number of request retries, number of captchas hit, etc. Storing these values is not always necessary, but can be valuable when debugging and maintaining an actor.

Before moving on, give these valuable resources a quick lookover:

-   Refamiliarize with the various available data on the [Request object](https://sdk.apify.com/docs/api/request)
-   Learn about the [handleFailedRequest function](https://sdk.apify.com/docs/typedefs/cheerio-crawler-options#handlefailedrequestfunction)
-   Ensure you are comfortable using [Key-Value stores](https://sdk.apify.com/docs/api/request) and [Datasets](https://sdk.apify.com/docs/api/request), and understand the difference

It's important to note that though we can already receive and pass to our storage some important data from the request object (such as `retryCount` and `errorMessages`), or from the handlePageFunction, we can also create store our own custom values.

### Quiz

1. Why might you want to store statistics about the run/a request?

2. In your Amazon scraper, we are trying to store the number of retries of a request once its data is pushed to the dataset. Where would you get this information? Where would you store it? Why?

3. We are building a new imaginary scraper for a website that sometimes displays catpchas at unexpected times, rather than displaying the content we want. How would you keep a count of the total number of captchas hit for the entire run? Where would you store this data? Why?

4. Is storing these types of values necessary for every single actor?

### Exercise

Heading back to your original Amazon actor (or any actor you are currently potentially building), complete the following tasks:

-   Each dataset result must have the following extra keys:

```JSON
{
    "dateHandled": "date-here", // the date + time at which the request was handled
    "numberOfRetries": 4, // the number of retries of the request before running successfully
    "currentPendingRequests": 24 // the current number of requests left pending in the request queue
}
```

-   An object including these values should be persisted during the run in the default Key-Value store:

```JSON
{
    "errors": { // all of the errors for every request path
        "some-site.com/products/123": [
            "error1",
            "error2"
        ]
    },
    "totalSaved": 43 // total number of saved items throughout the entire run
}
```
