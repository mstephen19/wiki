### Introduction

Public actors are **so** important to us that they are the first thing you see on our website. They follow the principal programming rule of "Don't repeat yourself". Once you've done something well, you should continue to reuse it _(as opposed to constantly reinventing the wheel)_. Public actors should be generic and flexible solutions that support the most important use-cases.

As a marketplace developer, you may be asked to create a specific configuration or integration of public actors. You can then use the specific configuration options to customize the actor's run, and let it hard work for you.

### Reading

- Scroll through the [Apify Store](https://apify.com/store) for a while and read through a few public actors. Look what they have in common and how they differ from each other.
- [Article about the versioning of public actors](https://help.apify.com/en/articles/3202824-versioning-of-public-actors)
- [Read the docs](https://apify.com/docs/actor#publishing) about publishing actors.
- Look at the **Publication** tab of your actor.

### Quiz

1. Do you need to build public actors to use them?
2. What is the term for an actor with a specific version?
3. Do you need a specific type of proxy to use Google Search Scraper? If yes, which type? How does it work?
4. How can you set up a country to scrape from in Google Search Scraper?
5. What type of authentication/authorization does the Google Sheets actor use? What are its benefits?

### Exercise
For this exercise, we will build a new actor which utilizes 3 different public actors: [Google Search](https://apify.com/apify/google-search-scraper), [Instagram Scraper](https://apify.com/jaroslavhejlek/instagram-scraper) and [Google Sheets](https://apify.com/lukaskrivka/google-sheets). Our new actor will simply call these public actors in order, and pass the data around as needed.

- First, it should call `Google Search Scraper`, searching for the query `instagram sport`. Scrape only the first 10 results. Additionally, filter only organic results, and add a rank from 1 to 10 to each result.
- Next, use the URLs of those scraped Google results to call `Instagram Scraper`. Use only `Direct Instagram page URLs`. `What to scrape from each page` should be `"Posts"`, and `Max Items` should be 10. This should give you approximately 100 results.
- Finally, enhance the Instagram data with `googleRank`. You need to correctly map the Instagram results to the Google results. Finally, create a new spreadsheet (on your regular email) and upload the data there using the `Google Sheets` actor. You can use `replace` mode. You will need to authorize it on the first usage. Make the spreadsheet public and add a link to it to the `README.md` of your Google Search + Instagram actor.