### Introduction

You already know about actors from the previous module, and also by simply trying stuff in the Apify app. Within this module, we will take a more in-depth look at actors. "Apify Actors" is basically a serverless platform that is running multiple Docker containers, which runs on AWS instances in the UE-east location.

### Learning

-   [Public actors page](https://apify.com/actors)
-   [Docs about running actors](https://apify.com/docs/actor)
-   [The absolute basics about Docker](https://docs.docker.com/engine/docker-overview/) (15 min read)
-   [Apify actor Dockerfile docs](https://sdk.apify.com/docs/guides/docker-images#example-dockerfile)
-   [Actor webhooks](https://apify.com/docs/webhooks)
-   **(BONUS)** If you are an enthusiast, you can download Docker (on Windows you will need the PRO edition for it to work smoothly) and play with it

### Quiz

1. How do you allocate more CPU for your actor run?
2. How can you get the exact time when the actor was started from within the actor?
3. What kinds of default storages an actor run gets allocated (connected to) by default?
4. Can you change the allocated memory of a running actor?
5. How can you run an actor with Puppeteer on Apify with headful(non-headless) mode?
6. Imagine the server/instance the container is running on has 32 GB and 8 cores CPU. What would the most performant (speed/cost) memory allocation for a CheerioCrawler? (hint: Node process cannot use user-created threads)

### Excercise

Let's return to the actor from the previous module and do the following:

-   Add a webhook that gets called after the successful run
-   Call another actor (which you will create), which will take the data and process them so for each product, only the cheapest offer remains.
-   The new actor will push the data to its default dataset. (No need to send an email this time, so feel free to comment that part of the code out)

In the next module, we will look more in depth into how to develop locally and manage your source code.
