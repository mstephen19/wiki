**There are two main ways to evaluate a developer's quality: _reviews_ and _experience rank_. Experience rank is a number between 0 and 5 that should tell how experienced a developer is in completing projects on the marketplace.**

### What's the difference between reviews and experience?

**Reviews**
- Submitted by customer
- Average stars can go up and down
- Customer will review the whole process - bidding, communication, code, how easy the final solution is to use, etc.
- Reviews will accumulate, every review counts

**Experience**
- Increased by Apify expert
- It can go only up
- Focuses mainly on your coding skills and ability to finish the project successfully
- You can always increase it with enough effort

Because Apify marketplace is a managed type of marketplace, Apify wants to add their insurance that developer that wants to take the job has sufficient skills and experience to finish the project.

### How does experience rank help or limit a developer?

- A developer can only take projects with complexity up to his or her experience
- Developers with higher experience can be in a better position to get a good deal

### How can a developer increase his experience rank?

- After finishing the base part of the tutorial, the developer will receive an experience rank from 2-4 depending on his or her performance.
- After every successful (defined below) project the developer's experience will increase by 0.1. This increase will happen only if the complexity of the project is at most 0.5 below the developer's experience. For example, if the developer has experience 3, he or she will need a project with a complexity of at least 2.5 to have his experience increased to 3.1. 
- After finishing 2 advanced tutorial modules, the developer's experience will increase by 0.1. This can only increase the experience to up to 4.

### What counts as a successful project?

Data-expert of the project will decide if you completed the project successfully and will increase your experience. A successful project should:
- Be fully working according to the specification
- Be delivered before the deadline (it can be extended if the problem was not on developer's side)
- The developer was able to complete the project with a little or no help

Keep in mind that reviews **will** become more important and prominent on a developer's page over the time.



