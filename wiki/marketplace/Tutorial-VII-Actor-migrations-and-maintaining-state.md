**This is the last module before your first real project! If you've made it here, it means you have true dedication to becoming an actor developer. Don't give up now, this will be short!**

### Introduction

Apify actors are a very generic and flexible platform. An actor is basically just a container that can be run on any server. That means actors can be allocated anywhere where there is any free space, making then very efficient. This all has, unfortunately, one big caveat - actors move a lot. And when they move (we call it migration), the process inside them is completely restarted, losing everything in its memory. When this migration happens, we want to do so-called "state transition" which means saving anything we care about so we can start where we were before the migration.

### Learning

- [Refresh your understanding of actors](https://apify.com/docs/actor#state-persistence)
- Look at [actor events in the SDK](https://sdk.apify.com/docs/api/apify#module_Apify.events).

### Quiz

1. Actors have an option _(in "Settings")_ to `Restart on error`. Would you use this for your regular actors? Why or why not? When would you use this feature?
2. Migrations happen randomly, but by setting `Restart on error` and then throwing an error in the actor's main process, you can simulate a similar situation. Try this out and observe what happens. What changes occur, and what remains the same for the restarted actor's run?
3. Why don't you usually need to add any special code for normal crawling/scraping to manage to handle migrations? Are there any features in the SDK that basically solve this problem for you?
4. How can you intercept the migration event? How much time do you have after this event happens and before the actor migrates?
6. When would you persist data to the default Key-Value store vs to a named Key-Value store?

### Exercise

Back to our favorite Amazon actor (again)!

Let's say that, for some reason, you want to keep an object in memory where all of ASINs that you have scraped thus far (the ones you've pushed to the dataset) are keys, and the values are the number of offers you scraped from the product corresponding with the ASIN - For example:

```JSON
{
    "B079ZJ1BPR": 3,
    "B07D4R4258": 21
}
```

- Implement this feature into your Amazon scraper, and every 20 seconds, display **the most up-to-date version** of this object to the log.
- This object should survive actor migration. That means saving it to the Key-Value store, and then loading it up again upon restart.
- Elaborate on whether or not you can ensure that this object will stay 100% accurate, which means it will reflect the data in the dataset. Is it possible? If so, how?