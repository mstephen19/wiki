### Introduction

We've already touched on the the topic of the Apify CLI in the SDK tutorial, but here we will discuss it more in depth, as well as explore all of the the different methods and tools you can utilize to manage the source code of an actor.

We haven't yet arrived at the perfect solution for managing our source code, so we will take a look at some more optimal solutions. The usual case is that you develop the actor locally, then push/copy the code either directly to the actor on the Apify platform, or to some remote Git repository (we use Github for public projects, and GitLab for private ones).

### Learning

- Read the [Apify CLI docs](https://github.com/apifytech/apify-cli).
- Spend at least 15 minutes playing with and learning Git (unless you know it well) - [Git docs](https://git-scm.com/docs).
- Read [this article](How-to-connect-GitLab-with-Apify) explaining how can you connect private Gitlab repository.
- Explore the "Multifile Editor" in Apify actors.

### Quiz

1. Do you have to rebuild an actor each time the source code is changed?
2. What is the difference between pushing the code changes and creating pull request?
3. How does `apify push` command work? Is it worth using in your opinion?
4. Why is Apify CLI useful when starting out with a new project?

### Excercise

Let's get back to our spectactular Amazon-scraping actor and follow these steps:

- If didn't do it yet, download and log in to the Apify CLI (this is absolutely key)
- If you don't have the code for your actor locally, create a new actor and paste the code there to the `main.js` file. Try to run the code locally. Does it work properly?
- Initiate a Git repo and push the code to the GitLab project _(you should not include `node_modules` or `apify_storage` in your push)
- Switch your actor to use source code from the Git repo
- Build the actor on the platform, and run it. Does it still work properly?