### Introduction

Effectively bypassing anti-scraping software is one of the most crucial, but also one of the hardest to master skills of any actor developer. The different types of anti-scraping protections can vary a lot on the web. Some websites are not protected at all, some require only moderate IP rotation, and some cannot even be scraped without using advanced techniques and workarounds.

It is generally quite difficult to recognize the anti-scraping protections a page may have when first inspecting a it, so it is important to thoroughly investigate a site first, as anti-scraping measures can significantly complicate the development and delivery process of an actor. As your skills expand, you will be able to spot anti-scraping measures quicker, and better evaluate the complexity of a new project.

### Learning

-   Read the [main proxy page](https://apify.com/proxy) to get a general idea.
-   Read the [proxy documentation](https://apify.com/docs/proxy#home). It is quite long so you can skip most of the examples.
-   Read about the [proxy trial](https://help.apify.com/en/articles/2108625-free-trial-of-apify-proxy).
-   Read [this overview article about bypassing antiscraping software](https://help.apify.com/en/articles/1961361-several-tips-on-how-to-bypass-website-anti-scraping-protections).
-   [This article about handling blocked requests in PuppeteerCrawler](https://help.apify.com/en/articles/2190650-how-to-handle-blocked-requests-in-puppeteercrawler) and [this article about filtering out blocked proxies using sessions](https://help.apify.com/en/articles/2617780-filter-out-blocked-proxies-using-sessions) are also worth to read. We will look into proxy sessions more in-depth in one of the advanced modules.
-   Read about the [Session Pool](https://sdk.apify.com/docs/api/session-pool)
-   You can surf around the codebases for some actors on the [Apify Store](https://apify.com/store) to see if, and how, they utilize proxies.

### Quiz

1. What are the different types of proxies that Apify proxy offers? What are the main differences between them?
2. What proxies _(proxy groups)_ do users get with the proxy trial? How long does the trial last?
3. How can you prevent an error from occurring if one of the proxy groups that a user has will be removed? What are the best practices for these scenarios?
4. Does it make sense to rotate proxies when you are logged in?
5. Construct a proxy URL that will select proxies **only from the US**.
6. What do you need to do to rotate a proxy _(one proxy usually has one IP)_? How does this differ for CheerioCrawler and PuppeteerCrawler?
7. Try to set up Apify proxy _(any group, or auto)_ in your browser. This is quite useful for testing how websites behave with proxies from a specific country _(most are from the US)_. Were you successful?
8. Name a few different ways of how a website can prevent you from scraping it.
9. Do you know of any software companies that develop anti-scraping solutions? Have you ever encountered them on a website before?

### Exercise

As always, let's get back to our amazing Amazon actor and make it support these new features:

-   The actor should now use a proxy that should use only the `SHADER` (Proxies from USA 1) group. If this proxy group isn't available to you (which means your trial has expired), please let one of us know. The proxies being used by your actor should only be from the US.
-   Implement a trivial proxy session manager. Keep in mind this will need to be coded differently depending on if you are using CheerioCrawler or PuppeteerCrawler. Your proxy should use sessions. If you make a successful request, you should keep using the same session (means it will pick the same IP). Do this at max 5 times, then rotate the IP. If the request fails, rotate right away. Limit concurrency to 1 for simplicity.
