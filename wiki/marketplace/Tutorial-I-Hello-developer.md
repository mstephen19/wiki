# Hello Developer

Welcome to the Apify marketplace! You may be a programming novice whos main goal is to improve his skills and earn some money on the way or you might be a javascript veteran who just wants to enjoy the remote/project-based work we offer. It doesn't matter for us, we are glad for everybody who will help us and our customers to make the web more programmable and improve/enjoy on the way.

Before we start working together, we would like to get your expectations straight. On the marketplace, there will be plenty of projects (crawling or automation), from simple scripts to more complex solutions. We are giving these projects a number from 1 to 5 that we call `complexity`. One is the easiest and five is the hardest. We expect that you start with picking the easier projects and over time climb in your skills and pick even the hardest ones. We, on the other hand, will try to keep a few easy projects for you so you have always a place to start.

### You should know...

Before you begin working with us, we expect that you have a solid (not perfect) knowledge of these:

-   English language (reading and writing)
-   JavaScript (if you don't know what a Promise is or what `setTimeout` does, keep learning and come later)
-   Node.js
-   General web development (HTML, HTTP protocol, Chrome/Firefox/etc. dev-tools, CSS selectors, etc.)

You should know at least basics about these (and aspire to master them):

-   [Apify Platform](https://www.apify.com/docs)
-   [Apify SDK](https://sdk.apify.com/)
-   [Apify CLI](https://github.com/apifytech/apify-cli)
-   [Puppeteer](https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md)
-   [jQuery](https://api.jquery.com/) and/or [Cheerio](https://github.com/cheeriojs/cheerio)
-   [Git](https://git-scm.com/docs)

Over time, you should:

-   Use [our version](https://github.com/apifytech/apify-eslint-config) of [ES-lint](https://github.com/eslint/eslint) (software that helps you format the code and check your style)
-   Read our [knowledge base](https://help.apify.com/) articles (check there before you ask for help) and [internal wiki](https://gitlab.com/apify-private-actors/wiki)
-   Try the most popular public actors from [library](https://www.apify.com/library)
-   Check what's new on Apify and implement the most up-to-date best practices in your projects

We also expect you to behave:

-   Politely
-   With respect
-   Professionally

Be sure to check the [rules](rules) to learn about the standards we expect from you.

## What we can offer to you

It wouldn't be a mutual relationship if we only wanted something from you. Here is what we are prepared to offer to you:

-   A steady supply of new projects
-   Technical and non-technical support on Slack
-   We will moderate the marketplace and try to solve any misunderstanding or conflict between you and our customers
-   Documentation, guides, and webinars for you to improve your skills
-   A friendly and no-bullshit environment
-   And more!

## Tutorial

Before taking on any real project for a customer, you should go through the tutorial that we have prepared to help you get familiar with Apify and our best practices. The tutorial consists of multiple concise modules. Each of them consists of a learning/reading part, a quiz, and an exercise. At the end of the tutorial, you will deliver your first real project that will earn you your first reward.

The tutorial is split into two halves - basic modules, and advanced modules. The basic modules are mandatory to begin working with us, while the advanced modules will earn you points that will allow you to pick harder projects and give you a better rating for our customers. You can learn more about [experience rank](marketplace/developer-experience-rank). The basic modules should be done in order, but for the advanced modules, the order in which you complete them doesn't matter. Expect to spend anywhere from a few hours, to few days on the tutorial modules (depending on your overall experience). Web-scraping and automation are not trivial, so it should be clear that we are really mainly looking for serious longer term partnerships.

It may happen that during the tutorial, you (or we) will discover that the job is not interesting for you, or that you lack an essential skill that is required in order to continue. In this case, you can always come back later after you feel better prepared. Please keep in mind that this is a managed marketplace (unlike [Freelancer](https://freelancer.com)), so we will be overviewing the projects, and giving you suggestions, feedback, and help. If you are to regularly fail at meeting our standards (check [rules](rules)), we will give you suggestions and/or warnings, and later may even have to completely cancel our cooperation altogether.

We don't expect you to answer all of quiz question perfectly - in fact, some of them are very opinion-based; however, we would like to see that you gave them some thought, and put real effort into answering them. The same goes for the exercises: it is not the end of the world if you skip or fail some parts, as we will be there to give you hints on what to improve. At the end of the tutorial, you will receive an experience rank based on your performance. If we conclude that you didn't put in enough effort, or that you lack certain essential skills, we will not offer you the place in the marketplace. On the other hand, if you manage to do exceptionally well, you will be able to start with more complex projects right away.

Before you jump into specific modules, try to spend at least an hour surfing around our [website](https://https://apify.com), [app](https://my.apify.com/), [store](https://apify.com/store) and [help section](https://help.apify.com/). Try to run a few public actors and examine the inputs/outputs, look at the codebases, try to play with the JavaScript editor, and read a few articles. Doing this will help you to get a better feeling of what our company is.

Now let's begin with the first module: [Apify SDK - the bread and butter of your toolset](marketplace/tutorial-ii-apify-sdk).
