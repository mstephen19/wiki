## Welcome to the first module. Let the learning journey begin!

Each module will consist of four parts:

-   Introduction
-   Learning
-   Quiz
-   Exercise

Firstly, you will learn about certain features, then you will be asked to answer some basic questions, as well as complete a simple task/mini-project. Even though each module is separate, you should only email us twice - when you have finished _Tutorial IV_, and when you have finished the final module. We will give you feedback on all the modules you have finished so far.

### One more note for managing the source code for exercises and quizzes...

As part of the onboarding, we have created your personal repo where you will push all your actors and quiz answers. Each actor should be one folder in your repo. The quiz answers should be in README.me, we will add feedback to the readme so we will create some kind of discussion over your answers there.

Read [this article](How-to-connect-GitLab-with-Apify) about how to connect your Gitlab account and repo with Apify and also how to load from different folders.

### Introduction

Now let's get to the real deal - Apify SDK. Since the SDK is so essential to your work, this module will be probably longer than the others. On the other hand, you should aspire to know it "in and out".

The Apify SDK is an open source JavaScript library we build on top of these technologies:

-   Apify actor (Runs the Node.js app built with SDK in a Docker container)
-   Apify API/Apify client (on the Apify platform)
-   local OS (for local development)
-   Puppeteer + Playwright (as browser managers)
-   Request + Cheerio (downloading and parsing HTML)

The SDK factors away and manages the hard parts of the scraping/automation development under the hood, such as:

-   Autoscaling
-   Concurrency
-   Request Queue + Request List
-   Storage
-   Simplified proxy interface
-   Simplified Puppeteer/Playwright setup
-   And more!

Your goal as a developer is to take this generic library and write website-specific or use-case specific actors. You shouldn't try to work around the SDK unless there is really no other way. If you find any limitations or bugs, please report them to the _#scraping-hotline_ channel.

The SDK and its resources can be found in various places:

-   Documentation - [https://sdk.apify.com/docs/api/apify](https://sdk.apify.com/docs/api/apify)
-   Guide - [https://sdk.apify.com/docs/guides/motivation](https://sdk.apify.com/docs/guides/motivation)
-   Examples - [https://sdk.apify.com/docs/examples/basiccrawler](https://sdk.apify.com/docs/examples/basiccrawler)
-   Github (source code, issues) - [https://github.com/apifytech/apify-js](https://github.com/apifytech/apify-js)
-   NPM - [https://www.npmjs.com/package/apify](https://www.npmjs.com/package/apify)
-   Help articles - [https://help.apify.com](https://help.apify.com)

### Learning

To complete this module, please do the following:

-   Read [the SDK guide](https://sdk.apify.com/docs/guides/motivation) and ideally **code along**.
-   Read [the SDK examples](https://sdk.apify.com/docs/examples/basiccrawler).
-   Read this article about [request labels](https://help.apify.com/en/articles/1829103-request-labels-and-how-to-pass-data-to-other-requests) and the [`userData` key on requests](https://sdk.apify.com/docs/api/request#requestuserdata).
-   Spend at least 15 minutes reading these documentation pages (unless you have already mastered the technologies) - [Puppeteer](https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md), [jQuery](https://api.jquery.com/).
-   Read [this article](https://blog.apify.com/what-is-a-dynamic-page/) about dynamic pages

### Quiz

Please answer these questions:

1. Where and how can you use jQuery with the SDK?
2. What is the main difference between Cheerio and jQuery?
3. Where would you use CheerioCrawler, and what are its limitations?
4. What are the main classes for managing requests and when and why would you use one instead of another?
5. How can you extract data from a page in Puppeteer without using jQuery?
6. What is the default concurrency/parallelism the SDK is running?

### Exercise

Please create a mini sample-project meeting the specifications outlined below. You may explain some of your decisions regarding performance, data quality, resiliency, and naming; although, there is no need to over-optimize this simple task.

-   The actor will receive input in this format, where the keyword will vary:

```JSON
{
    "keyword": "phone"
}
```

For our test case throughout these first few modules, we will use this URL to search Amazon with your actor: `https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=iphone`

Then, you will:

-   Get all ASINs from the first page of results (the ASIN is the ID of the Amazon item, which looks like "`B00YD545CC`")
-   Go to the detail page of the offer (the product), and the save its description
-   Continue to the offers of the listing with a URL which looks like this - https://www.amazon.com/gp/offer-listing/B00YD545CC
-   Scrape and push all offers to the dataset. Add the `title`, `itemUrl`, `description` and `keyword` values from the previous steps to each offer.

The final output should look like this:

```JSON
[
    {
		"title": "Apple iPhone 6 a1549 16GB Space Gray Unlocked (Certified Refurbished)",
		"itemUrl": "https://www.amazon.com/Apple-iPhone-Unlocked-Certified-Refurbished/dp/B00YD547Q6/ref=sr_1_2?s=wireless&ie=UTF8&qid=1539772626&sr=1-2&keywords=iphone",
		"description" : "What's in the box: Certified Refurbished iPhone 6 Space Gray 16GB Unlocked , USB Cable/Adapter. Comes in a Generic Box with a 1 Year Limited Warranty.",
		"keyword": "iphone",
        "seller name": "Blutek Intl",
		"offer": "$162.97",
		"shipping": "free"

    },
	{
		"title": "Apple iPhone 6 a1549 16GB Space Gray Unlocked (Certified Refurbished)",
		"itemUrl": "https://www.amazon.com/Apple-iPhone-Unlocked-Certified-Refurbished/dp/B00YD547Q6/ref=sr_1_2?s=wireless&ie=UTF8&qid=1539772626&sr=1-2&keywords=iphone",
		"description" : "What's in the box: Certified Refurbished iPhone 6 Space Gray 16GB Unlocked , USB Cable/Adapter. Comes in a Generic Box with a 1 Year Limited Warranty.",
		"keyword": "iphone",
		"sellerName": "PLATINUM DEALS",
		"offer": "$169.98",
		"shipping": "free"
    },
	{
		"...": "..."
	}
]
```

After the scrape is done, call a [public actor which sends emails](https://apify.com/apify/send-mail), and send an email to `lukas@apify.com` with your name, a subject saying "This is for Apify SDK excercise", and public link to the dataset.

And that's it! We will use the SDK in the next modules too. In the next module, we will focus on **Apify actors**!
