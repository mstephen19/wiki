These rules should help you as a developer, us as data-experts and a customer to maintain a sort of contract of expected communication and delivery timeline. We gathered these rules based on our past experience and we mainly want to avoid unnecessary delays or misunderstandings of the specification.

Our data-experts will observe if you follow these rules and it may influence who will get better access to new or special projects or who we would like to keep for the long term.

### Communicate often
No matter if you communicate directly with the customer or through a data-expert, you should provide a continuous update on where the project stands so you can receive feedback often and adapt if necessary. Also, there is no shame in telling that you didn't have time to do anything in the past days, it is certainly better than silence.

When working for a data-expert, he should tell you how frequent feedback he expects from you but generally, it should be every 2-3 days.

When communicating with a customer directly, the frequency should change depending on the deadline and the need of the project (some specs are more clear than others). For projects with a short deadline (1-2 weeks), provide feedback at least every 2 days. For longer projects, up to 4 days interval is fine.

### Pick project only if you can start in upcoming days
Don't hoard projects, there are other people who want to pick them. If you pick a project, we expect you to start working on it at worst in the next two days.

### Provide sample data asap
You should write the crawlers/automations in a way that you can provide sample data/runs as soon as possible. It is much better to get a continuous update on the data quality than to finish with a huge but wrong dataset. You should always keep at least a week (or few days in terms of a short deadline) for a debugging (don't underestimate this). Start with a small category or even just one product.

### Keep the tests short, don't consume a lot of CUs
When you are testing the actors, there is usually no need for long runs. If you want to test data quality, just run a short run with hundreds of pages. If you want to test how many items you can gather, set up a flag so they are just enqueued, not scraped. Ideally, you should do the full run just once, before providing it to the customer. Long runs with consuming more than thousand of CUs cost us hundreds of dollars.

### Don't "shout" the updates, write a nice paragraph
When communicating with the customer, you should always introduce yourself first and then write a few sentences about what you did in the past days and what you will do in the upcoming days. The same goes when you communicate with your data-expert. Avoid sending multiple short messages

### Solve problems systematically
You will encounter problems of various difficulties on the way. We expect you to be able to solve most of them. As a first thing, you should always check if solving the problem requires (or makes more sense) you to change the specification. If yes, you need to agree on it with the customer or a data-expert. 

When finding a solution, try to first solve it on your own. If it is a problem with Apify platform, read the docs or the tutorials. If it is a programming problem, just Google it, you may find answers on Stack Overflow or some npm package that solves it. Don't underestimate the power of console.log, screenshots or live view. Play with it, you will learn from trial/error. Only after you spend some time trying, you may ask on `scraping-hotline` Slack channel for feedback from experienced developers. If you get a feeling that the project is out of your current abilities, there is no shame in unassigning from the project. You just need to inform the data-expert and the customer.

### Make sure you understand the specification
Asking is always better than guessing or pretending to know. You shouldn't change the specification unless there is a real need for it and then you need to discuss it with the customer or data-expert.

### Follow the Output format
If the project specification contains any output sample or format (JSON or table), you should follow it exactly. Check your types, they need to match exactly (parsing numbers, arrays, nested objects, etc.). This is especially important in bigger projects where more websites are done for the same client.

If there is some part of the output that you think should be changed or makes more sense to do differently, you need to agree with a customer or data-expert.

If there is no specified output, it is always better to agree with the customer or data-expert in the beginning (either create one manually or as a fast sample) than hope that customer will agree on your final output.

### Test also on Apify platform
Developing locally is a great way to start the project. But once you have something done, you also need to test the same code as an actor on Apify platform. It can behave differently there for a bunch of reasons so you need to make sure it is still working properly before sending it to the customer or data-expert.

### Handle actor migrations and restarts
Actors are by definition unstable and migrate often through servers. In that case, you lose what you have in memory but keep the storages. You need to save data from your memory and load them on the restart. If you are using requestQueue or requestList, the requests are handled for you but for other data you need to keep the state through restarts. This is important for all actors that take more than a few minutes! Also, ensure that if restart happens anytime, no data will be missing or duplicated and the actor will not repeat the steps it already done before restart. Of course, this cannot be taken as an absolute statement since some smaller steps may be easier to repeat than to keep some complex state (but it should be a general rule).

### Use Apify SDK classes and methods
Don't reinvent the wheel. Use the standard, it will be more readable and maintainable. Instead of looping over URLs, use a request queue and one of the crawler classes. Even if you need to scrape just one URL, crawler classes will give you automatic retrying and a few other nice features.