Every developer has its own style. And his style can be even evolving as he grows as a developer. 
Someone can prefer more [functional](https://en.wikipedia.org/wiki/Functional_programming) style, while others find [imperative](https://en.wikipedia.org/wiki/Imperative_programming) code easier to reason about. That is fine and we at Apify don't force our developers to a strict standard. But while we give you a freedom to write a code that makes sense for you we also need to make sure that the actors are written well. The reason is not only that the code should simply work and produce desired results which is the bare minimum but that the code may be later checked by your data-expert, looked through or even changed by the customer or even fixed/updated by another developer. Of course, there are actors where non of this will happen and you could have successfully finished the project even with horrible code-mess but over the longer timescale, writing proper code will save much more time than it takes to write it.

In this article I would like to lead you on a path to writting actors that will be easy to read and understand, easy to change and will communicate your intentions. Of course, these are not rules set in stone and you should always evaluate if using these standards make sense for your project. You should think about this path as an ideal that your code should be aspiring too although it cannot ever be perfect. I will state few rules and ideas that may help you stay and advance on this path.

## Code Style
- Praise [clean code](https://blog.risingstack.com/javascript-clean-coding-best-practices-node-js-at-scale/). In short, use proper variable and function names and split your code into smaller chunks/functions (ideally [pure](https://en.wikipedia.org/wiki/Pure_function)).
- Define any constants that apply to the whole actor either on top of the code (after requires) if using just one file or create a file `constants.js` where you will import them from. Constant names should be written in UPPERCASE_WITH_UNDERSCORES style.
- In hand with that goes: Don't use any [magic numbers](https://en.wikipedia.org/wiki/Magic_number_(programming)) (usually options like `maxRequestRetries` for crawlers). If it makes sense, let the user set them in the input. If not, declare them as constants.
- Don't be shy to use comments in your code. Especially comment the blocks where you had to make some decision or where you chose unusual solution. You may even comment few commonly used things to help the customer understand it.
- Use modern ES6 and newer syntax. Replace `var` with `const` or `let`, `indexOf` with `includes` etc.

## Logging
- Usually more logs is better than less logs. If you code a crawler, log when the crawlers starts and when it finishes. Log each URL as the page loads (you may even add index and time logs for the URLs). Especially log your catch blocks, no error should pass unseen unless you have a very good reason to that. Just keep in mind that the full actor log has some maximum length and for longer running actors it may become trimmed which is not desirable so for bigger and longer actors be more convervative and rather use more of a next point.
- For longer runs set an interval that will log some stats like pages opened, pages successfuly pushed, errors etc.
- The meaning of the log should make sense to the outsider. Avoid log lines with just numbers or URLs - always add a meaning to each line.
    - Wrong: ```300 
        https://example.com/1234 
        1234```
    - Better: `Index 1234 --- https://example.com/1234 --- took 300 ms`

## Input
- If you allow user to pass into input something that could break the actor (e.g. minConcurrency of crawlers set to 1000) and if user uses an unreasonable value either throw an error or choose a maximum value and log a warning.
- Validate the input. If the fields in the input are missing or are in a bad type/format, throw an error as soon as possible!

## Settings
- Use a meaningful timeout, not 0 or 9999999. It should enable any valid run to finish without any stress but it should ensure that any hanging run (run that got stuck and running infinitely) is timeouted.
- Use proper amount of memory. Consult the need for speed with the data-expert (the more memory you allocate, the more CPU). Some actors not doing any CPU jobs could run even with the lowest memory (especially if they are just starting other actors and waiting). For Puppeteer minimum memory is 512 MB, maximum is unbound. But for pure http requests + cheerio, the best performance/memory is on 4 GB because that's where one CPU core is allocated and cheerio can run only on one core.
- Unless you have a reson to, don't use `restart on error` option. It can lead to looping fails which can hide the actual error that needs to be dealt with.
- Be careful with `use spare CPU capacity`. Firstly don't use it when testing speed and performance because it will not yield consistent results. Secondly some longer runs can have problems with rapidly channging CPU power and autoscaling so be mindfull of that. 

## Error handling
- If an errors occurs, it should be obvious from the log why and where it happened.
    - Wrong error log: `Cannot read property “0” from undefined`
    - Better: `Could not parse an address, skipping the page. Url: https://www.example-website.com/people/1234`
- Try to cath as many errors as possible and update them with additional information.
    - Instead of just: `Node is either not visible or not an HTMLElement ...`
    - Catch it and rethrow with more information: `Clicking the publish button failed with error: Node is either not visible or not an HTMLElement...`
- On the other hand, sometimes the default error log has enough information and the error is handled for you (e.g. in SDK's crawlers) that introducing another try/catch block would just bloat the code unecessarily. You will have to decide what makes more sense but always think about the person reading your log.
- Error catching should be done in meaningful blocks. Crucial or error prone parts (like an API call or specific matching pattern) should be caught sepatately but blocks of code where only unexpected error can happen can be caught as a whole.
- If an error occurs that essentially make the output of an actor useless, unless you have a reason not to, let the actor fail right away.
- If you are catching errors, try to make sure you are doing more good than bad. If you catch an error and not even log it, noone will later notice and it can cause unexpected problems later. Logging should be bare minimum but think about what state should be your actor in after the error occurs. There are generally 3 states an error can produce:
    - The error doesn't change the overall state of the actor. => Actor can continue normally after the catch block. `E.g. We could not parse one variable but the data are good even without it.`
    - The error changes the state of the current block of code. => Usually we want to throw an error and the current function should be either retried and abolished.
    `E.g. Something important on the current page crashed (e.g. We receive a captcha) and we couldn't not push the data in proper quality`
    - The error changes the state of the whole actor. => This is a fatal error and usually we should exit the whole actor with a non-zero exit code (signalling failed run).
    `E.g. Credentials stopped working or we could not parse required attribute from input`

## File structure
- Simple actors should be done as a one file unless specified otherwise. For complex actors and some enterprise customer, discuss with your data-expert as we are hosting some of these on gitlab and you would be then using multifile format with git commits. These multifile actors should be then written close to what is specified in for [public actors](https://gitlab.com/apify-private-actors/wiki/wikis/public-actors).