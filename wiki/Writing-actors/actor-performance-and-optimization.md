There is quite a paradox in creating actors for users. The users pay us for the compute power the actor consumes so there is actually an incentive for us to create bad code to get paid more and spend less time preparing it. Although this could be a viable short-term strategy, we think it is simply better to write readable, performant and bug-proof actors. In the long term, users will value that they are getting more value for their bucks, we look more professional and we also grow together as developers. 

This doesn't need to be taken to an extreme. Spending a few hours optimizing a simple use-case just to get few percents of speed is probably not worth it unless the customer is expected to run a huge volume. So when evaluating how much time spend on optimization, a users use-case and potential have to be evaluated. But readability, performance, and correctness should be something that we generally strive for.

## General issues and ideas

### Watch for memory/CPU leaks

If the average memory usage of your actor is slowly creeping up, your actor is slowing down and everything starts to crash there is a chance you are dealing with memory or CPU leak. A memory leak means that there are more and more data stored in the actors memory eventually crashing the whole process. CPU leak is mainly slowing down the process/processes until they usually just stop, hang or crash. These two are very often connected and can have the same cause. 

The cause can be found in the user code: An evergrowing global object that cannot be garbage collected, growing number of asynchronous tasks that weren't properly finished (think setIntervals or hanging requests) or browsers/pages not closed when using Puppeteer. These are just a few examples of infinite possibilities how a user can screw up the actor. But fixing these is actually the easier part of the possible problem since you can see them directly in your source code. Worse is if the leaks are generated in some package or library that you import to your actor. In the past, there were such leaks in the Apify SDK. In this case, it may not be so easy to find where exactly the leak is happening and also you cannot usually directly change the code (at least without spending a lot of time). In this case, the best approach is probably properly document where and how the leak is happening, first checking if it is not really caused by the user code, and then either filling an issue in the library's repo or just abolishing it completely.

**Common issues and their solutions**
- Not closing browser/page in Puppeteer when code crashes (doesn't apply to puppeteerCrawler since it handles browsers/pages for you). **Solution**: Put the code inside try block and close the browser/page in the catch block.
- Storing data into a variable that cannot be garbage collected. **Solution**: Depends if you need the data or not. If not, simply put the variable in the correct scope/block (this can be tricky so read more about memory leaks). If you need the data but not as a whole, simply store it somewhere (kv store or dataset) and then reuse it as needed. If you actually need to the continuously, try to limits its size as much as possible and then increase the memory as a last resort.

## Web crawling and scraping

### Use a browser only if necessary

Although using a browser (mainly with Puppeteer) for any use-case may be convenient, the performance price is sometimes just too big. Browsers eat up a lot of CPU by executing client-side javascript and rendering the page and they can be even 20 times slower with the same CPU/memory allocated when comparing with pure HTTP requests. And the reality is that pure HTTP can be used in a lot of cases so our ideal should be using a browser only when pure http would simply not work.

The cases when to use pure http or browser are not always so clear. On one side, you have a simple page that has just all the necessary information in the html document and loads just a few images in the background that we don't care about. On the other side, you have a heavy javascript website with hundreds of additional requests running from the page and it doesn't actually load anything with enabling javascript (which requires a browser). But most of the websites are not one of these extremes. They use some javascript but do not strictly require it, they may do tens of requests but these can be replicated and so on. For these websites, it is important to evaluate exactly which data we need from the page and which paths (which resources loaded) needs to be taken in order to get the data needed.

Generally, you don't need to use a browser in more cases than you originally imagine. Even if the website loads with javascript, it can return the javascript inside the html and you can parse the data from the scripts. You can simply get the html with your http client, search for the data with CMD/CTRL + F and then decide if it is feasible to get them out of there. The same is true for xhr requests (internal requests between the page and a server), even if they are originated with for example a click or a scroll, in some cases, you can replicate them with clever usage of headers and query parameters/post payload. The main issue than can be cookies or any other tokens that may be hard to get from the pure response.

Another important idea is that it not one or other but you can combine both browser and pure http in one crawler. It can happen that some pages of the same website will require a browser and others will not. You can use requests directly from your handlePageFunctions or use a more generic basicCrawler and handle the browser/pages yourself.

Anyway, as we said earlier, it is important to evaluate the cost-benefit analysis of an endeavor to use pure http. Usually, it makes sense if possible but there are cases when it can cause more troubles than produce benefits. In some cases, pure http may also become blocked more often than a browser.

### Prefer fetch($.ajax, xhr) over interaction with the page

Even when pure http requests don't work or work badly, not all is lost. You can use requests directly from the client side code (from a browser) with a native [fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) function (or jQuery's ajax or native XmlHttpRequest). The advantage is that they will automatically (in fetch you have to specify `credentials: "include"` options) send proper cookies, headers and will have the page's origin. 

Since the page and the server endpoints should be optimized for working well together, you should get the data back quickly and securely. These calls are usually used internally when you engage with the website by clicking, scrolling etc. By using them directly, you can save a lot of time and sometimes you can find high-quality data that are not displayed anywhere on the page. Sometimes the data are "prefetched" for multiple pages and then you can also save a lot of loading and navigation time. Look through the network tab in the developer's console, the most interesting things happen usually with the xhr requests.

### Catch data directly as they come in rather than replicate the xhr requests or wait for the full page content to render

Some pages that are dynamically loaded with xhr requests take ages to load fully. There are multiple approaches how to wait for the data and extract it but they differ in speed.

- Wait for the data to render on the page and then scrape them from the html
- Replicate the xhr request that is make to load the data and use it separately to get the data directly
- Wait for the data to come in and extract them in the same time they arrive

All of these approaches should lead to the same results most of the time and the speed difference should not be huge but when optimization is really important, the last approach wins. Problem with the first approach is that you first wait for the data to arrive, then you have to wait for the data to render and then you need to extract them from the html - that is 2 unnecessary steps when we "already got" the data. The second approach could theoretically have a speed bonus since you can make it right away but most likely the server would still take its time to deliver the responses in its predefined order and you spend some time coding the proper request. But in some cases it can make sense. The last approach is usually the optimal one since it doesn't require much additional code (just waitForResponse and extract data needed from it) and gets the data as they naturally arrive so we don't mess with how the page works.