Even though we would maybe want to give perfect support to everybody, in real life we have to prioritize. The most important metric for us should be how much money can this customer earn us. It can be by subscribing to a paid plan and/or by requesting projects. You should value what is our margin on this, e.g. customer on the Freelancer ($49) who is constantly asking for something has a much smaller value than someone who stays on the same plan but contacts us only with important matters. The same applies to custom projects - more valuable customers can give us higher margins.

Let's define a simple equation which we will use to evaluate how much effort to spend on the customer:

**Customer value** = *Expected revenue - time spent to support*

Customer value should then lead our priorities. We will give a few examples below.

#### 1. Location
Customers from richer countries are of course more willing to pay for the services they need. Just be careful that it is only tracked by IP address. If they use a proxy or they are traveling, it will be misleading. Of course, we also have very valuable customers from poorer countries so this is just a tendency.

*Poor countries <- Customer value -> Rich countries*

#### 2. The customer has a clear idea of what he or she needs
A lot of leads are just kind of hanging around and asking about Apify thinking if they could use it for something. That is fine but from our experience, these guys are not so valuable since they are not so motivated and waste of a lot of time with broad questions. On the other hand, a customer that has very specific use-case is highly motivated to have it solved (and subscribe).

*Broad/unclear use-case <- Customer value -> Specific use-case/clear specification*

#### 3. How urgent is the need to solve the problem
Customers who have an urgent problem are often able to pay much higher prices than they usually would. Also, they don't have so much time to evaluate offers from different vendors so there is a bigger chance they will stick with us. But there is one caveat - they also create a lot more pressure on us and this needs to be accounted for. If they need fast delivery, they should pay a premium for that.

*Long-term evaluation <- Customer value -> Urgent need (only if they are able to pay a premium)

#### 4. History of subscriptions
Customers who have been subscribed in the past or are subscribed continuously are more likely to keep generating revenue. Intercom allows you to set a detailed filter of what you want to see about the customer. This also should be integrated with our CRM in the future. High subscription and long term customers should have the most priority.

*Short history + low subscription <- Customer value -> Long history + high subscription*