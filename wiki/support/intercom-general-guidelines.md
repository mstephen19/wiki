In this article, we will overview several topics that you should be aware of when supporting customers on Intercom.

### 1. Chat vs email
Intercom handles chat and emails in a very similar way. It recognizes the customer whether he or she sends an email or writes to a chat. Of, course, if the customer clears cookies or changes emails, this doesn't work and that happens from time to time. Furthermore, emails going to support@apify.com are forwarded to Intercom. 

You should recognize what is the communication medium as both should be answered in a different way. Be careful that sometimes this changes in the middle of the communication as customers are switching from email to chat or the other way.

**Chat**
- Customers value a lot if you respond immediately, especially on the first message.
- Communicate in a shorter, more "chatty" live style so the customer feels you are here for him or her.
- If you need to leave, you should tell it to the customer so he or she is not waiting there. Tell him or her when he or she can expect a reply or provide your email (or your colleagues) for further communication.
- Don't be dragged into an endless discussion. The goal is to make a subscription.

**Email**
- No need to reply immediately, you can pass the email to a responsible person.
- Reply in a longer, more official style.

You can see how the message sent here:
**Chat**: 
![chat](uploads/e2d18a4800cd9fb1787da40abcad359c/chat.png)

**email**: 
![email](uploads/b9cb361549a3a73572080a72aa7b7653/email.png)

### 2. Evaluate who is the customer and react accordingly
Intercom's huge benefit is that it gives you a lot of information about the customer. Some information is based on Intercom's user tracking like `location`. Some is simply connecting the customer's email and cookies. And lastly, Intercom in also integrated with Apify so you can see things like current subscription value.

#### Lead vs User
Intercom shows you if the customer already created an Apify account. If someone is marked as a lead, you should expect they didn't see the app yet and did not try any actor. But be careful, these can be actual users writing from different email address/with cleared cookies.

![lead](uploads/540b0bf661157fd5860904f13548f2e8/lead.png)

For a lead, there are two basic paths. Either it is an enterprise customer looking for receiving a quote or project viability answers or it is someone interested in the platform and actors. 

#### Enterprise
You should answer all technical/billing questions for the enterprise customer so they understand how we work and if their project is possible(almost always is) but essentially they should send their specification over the email where sales and data-experts can continue. Sometimes they expect to receive an email first so you need to contact responsible persons on Apify team.

#### Wanna-be-user
For regular leads looking into scraping some data/automating their workflow, you should present them how to work with Apify and guide them to try some (well working) actor. Explain to them the sign up is for free and lead then to the shortest path where they can see data showing on their screen. Remember that customers are interested in data, not internal workings of our app (you can explain that later).

Actual Apify user can have a plethora of questions so we won't speak about them specifically.

#### Expected value of the customer
[Read here](support/customers-value) how to evaluate customer's value.

### 3. Look at the customer's history
Intercom allows you to see both long term and short term history (this may need some tweaking in the settings) of the customer. If he or she asks more than very simple questions, you should always understand the context in which he or she is asking. This makes the support faster and more professional. 

#### Recent page views
This section on the right shows you what the customer did so far in his session. You can immediately see what actor/task he is talking about. You can see what he or she tried and what was his activity. Sometimes you can see he got stuck. This can help you to ask more specific questions. If you see the customer got stuck, navigate him to the solution or try a different approach.

#### Latest conversations
This section is a bit below `recent page views` and it shows a full history (if you click through) of what the customer was dealing with. If the customer has specific questions about some actor you can often clearly see that he or she was dealing with one Apify expert and in that case, it is easiest to contact this expert and pass the support to him or her. Sometimes this may not be possible and then you need to evaluate the customer's value. For valuable customers, it may be worth to investigate the problem on your own. Otherwise, let them know the previous expert will contact them asap.

### 4. Be aware of toxic customers
Unfortunately, every support system attracts some types of toxic customers. They are defined as users who bring very little value and need constant attention. The net benefit for us is negative, they simply cost us money. [There](https://www.business.com/articles/5-customers-that-want-to-destroy-your-business/) is a very nice article about different types of toxic customers. 

The interesting realization is that it is not that they are just stupid or annoying people(maybe they are) but that the problem is the company's ambiguous presentation that attracts them and that they feed on. Basically, they are teaching us to be more clear and straightforward. If you show them weakness, the will feed on it and waste your time. If you approach them correctly, they can even become valuable customers. The key to that is to show very clear limits of what you can do for them. Show them our pricing page, our documentation or knowledge articles. Don't chat about their problems.

### 5. Use Saved replies
Intercom Saved replies are a great tool to save time and prevent typos. You can even add variables like customers or your name. Over the times we have created some stable ones to use and we try to manage them. Read through the current ones and if possible try to use mainly them (instead of adding your own). If you have any ideas or feedback for them, just let us know. Sometimes it is handy to create some temporary reply for an actual event or situation (big bug in the app) - don't forget to delete such reply after.

### 6. Have an overview of Apify help articles(knowledge base)
Intercom allows you to directly link the knowledge articles that are hosted there. Articles are a great way to answer technical questions as they save the time of the explanation and build customer's broader understanding of our service. You should know what are the articles about so that you can offer an appropriate one. It can be also useful to send them the basic tutorial articles, especially if they are new customers. Since the articles are often linked together, it will save a lot of possible questions.