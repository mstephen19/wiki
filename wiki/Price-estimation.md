## Price estimation

Price estimation is a process required to prepare a quote for a custom solution. It starts when a customer requests a solution and ends with a specific price offer. The process of price estimation includes asking for detailed specification, understanding the project scope and complexity and assessing customers budget. The ultimate goal of a price estimation is to prepare a quote with a price as high as a customer is willing to pay in the shortest amount of time with regards to the project complexity.

### Complexity

Complexity is a number from 1 to 5 that can be decimal. Complexity describes how much time and experience the project requires from the development standpoint. The higher the number is, the more difficult the project is. The difficulty scale is based on the average project difficulty in Apify's history. An average project has complexity 3. The important thing to understand is that the scale is non-linear. A project with complexity 4 is much more complex than two projects with complexity 2 together.

To asses complexity properly, a data-expert needs to evaluate all aspects of the project. Examples are:

- How hard is to write code to crawl all requires pages
- How hard is to write code to extract all required attributes from a page
- How hard it is to overcome anti-scraping solutions
- How big is the project volume (bigger scrapes inherently cause more problems)
- How clear is the specification and customer's communication (clear projects are much easier than unclear and require less time communicating)

Examples of complexity. Keep in mind that there is a continuous decimal scale in between these examples. The example time is for an average marketplace developer. Experiences Apify expert can have the times lower and novice higher.

- `1`: One-page scripts, simple HTTP request - `10 - 20 minutes` 
- `2`: Simple e-shop, no proxy needed, few attributes - `30 - 90 minutes`
- `3`: Average e-shop, requires parsing and pagination, has simple anti-scraping protection - `3-5 hours`
- `4`: Very complex website, database insert, hard bot protection, custom inputs and outputs, simple integration - `1 - 3 days`
- `5`: Basically a whole application, complex integration, various inputs, and outputs, requires maintenance, monitoring... - `7 days - one month`

### Roles

Price estimation process defines different roles that have their place on the whole process. The role is not bound to one person. Some roles may be absent or one person can have multiple roles.

- Sales - Sales is the role that handles the non-technical aspect of the estimation. His main focus is to build trust with the customer and assess customers budget options. Sales ultimate goal is to sell the solution for a high price a make the customer happy.
- Data-expert - Data-expert is a technical expert that understands the project's specification and is able to produce the complexity number after evaluating the specification and asking all necessary questions.  His ultimate goal is to produce a reasonably accurate complexity number is the shortest amount of time.
- Developer - Developer is the person who will develop the projects itself. His focus is to ask if he doesn't understand anything and let data-expert know when he finds an extra complexity that was not obvious from the initial specification so the price might need to be adjusted.

### Process

The process differs depending on the type of the customer and if he uses enterprise or marketplace solution.

### Platform usage

Although not directly part of the one-time price offer, a customer is very often interested is how much will the solution consume over time. A data-expert should be able to assess a very rough estimation. The roughness of these estimation differs depending on the project type. A data-expert role is to produce a good enough idea about the consumption but not spend much time on it. He or she should put as much work on the customer as possible - for example, to count the rough number of products on the website. In some cases, data-expert can have no experience with assessing consumption. In that case, he can ask colleagues or simply give up. 

The estimation can always be made more accurate as the developer is testing the project in the development process. He should be able to give feedback to the data-expert who can then inform the customer. Once the customer runs his project, it should be pretty obvious what will be his consumption.

#### Problem with compute units

Compute units are almost impossible to explain to a non-technical person. In this case, you can roughly compare them to the pages as in our pricing (e.g. 1 CU = 500 pages). This, of course, makes sense for the websites where crawling pages has a major role. If you know the solution doesn't require a browser and can be done with pure HTTP, it can about 10 - 20 times less consuming.

